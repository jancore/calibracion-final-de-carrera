#include "potentiometer.h"

#define INC 63
PotentiometerArduino poteE(12,11,10,INC);
PotentiometerArduino poteW(9,8,7,INC);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.print("Selecione un numero:\n1.Calibrar Este.\n2.Calibrar Oeste.\n3.Comprobar Este.\n4.Comprobar Oeste.\n");
}

void loop() {
  if(Serial.available() > 0) {
    char option = Serial.read();
    switch(option){
      case '1':
        Serial.print("\n1");
        poteE.decrementSequence(100);
        poteE.incrementSequenceAndSave();
        Serial.flush();        
        Serial.print("\nCalibración este terminada.");
        break;
      case '2':
        Serial.print("\n2");          
        poteW.incrementSequence(100);
        poteW.decrementSequenceAndSave();
        Serial.flush();
        Serial.print("\nCalibración oeste terminada.");
        break;
      case '3':
        Serial.print("\n3");
        if(poteE.checkCalibration())
        {
            Serial.flush();
            Serial.print("\nFC Este calibrado");
        }
        else
        {
          Serial.flush();
          Serial.print("\nFC Este NO calibrado");
        }
        break;
      case '4':
        Serial.print("\n4");
        if(poteE.checkCalibration())
        {          
            Serial.flush();
            Serial.print("\nFC Oeste calibrado");
        }
        else
        {
          Serial.flush();
          Serial.print("\nFC Oeste NO calibrado");
        }
        break;
      default:
        Serial.flush();
        break;
    }
  }
  Serial.flush();
}
