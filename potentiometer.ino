#include "Arduino.h"
#include "potentiometer.h"

PotentiometerArduino::PotentiometerArduino(unsigned int pin_CS, unsigned int pin_UD, unsigned int pin_FC_ANALOG, unsigned int maxIncrement) {
  _pin_CS = pin_CS;
  _pin_UD = pin_UD;
  _pin_FC_ANALOG = pin_FC_ANALOG;
  _maxIncrement = maxIncrement;

  pinMode(_pin_CS, OUTPUT);
  pinMode(_pin_UD, OUTPUT);
  pinMode(_pin_FC_ANALOG, INPUT);

  digitalWrite(_pin_CS, HIGH);
  digitalWrite(_pin_UD, LOW);
}

void PotentiometerArduino::setDelays(
  unsigned int tCSLO_us, unsigned int tCSHI_us, unsigned int tLUC_us, unsigned int tLCUF_us, unsigned int tLCUR_us, unsigned int tHI_us, unsigned int tLO_us, unsigned int tS_us){
  _tCSLO_us = tCSLO_us;
  _tCSHI_us = tCSHI_us;
  _tLUC_us = tLUC_us;
  _tLCUF_us = tLCUF_us;
  _tLCUR_us = tLCUR_us;
  _tHI_us = tHI_us;
  _tLO_us = tLO_us;
  _tS_us = tS_us;
}

/*Datasheet increment timing Waveform*/
void PotentiometerArduino::incrementSequenceAndSave(){
  int count = 0;
  digitalWrite(_pin_UD, HIGH);
  delayMicroseconds(_tLUC_us);
  digitalWrite(_pin_CS, LOW);
  delayMicroseconds(_tLCUF_us);

  while(digitalRead(_pin_FC_ANALOG) && count < 100)
  {
    digitalWrite(_pin_UD, LOW);
    delayMicroseconds(_tLO_us);
    digitalWrite(_pin_UD, HIGH);
    delayMicroseconds(_tHI_us);
    count++;
   }
    digitalWrite(_pin_UD, LOW);
    delayMicroseconds(_tLO_us);
    digitalWrite(_pin_CS, HIGH);

  delayMicroseconds(10000);
}

void PotentiometerArduino::incrementSequence(unsigned int n_increments){
  digitalWrite(_pin_UD, HIGH);
  delayMicroseconds(_tLUC_us);
  digitalWrite(_pin_CS, LOW);
  delayMicroseconds(_tLCUF_us);
  while(n_increments > 0)
  {
    digitalWrite(_pin_UD, LOW);
    delayMicroseconds(_tLO_us);
    digitalWrite(_pin_UD, HIGH);
    delayMicroseconds(_tHI_us);
    n_increments--;
   }
    digitalWrite(_pin_CS, HIGH);
}

/*Datasheet decrement timing Waveform*/
void PotentiometerArduino::decrementSequence(unsigned int n_decrements){
  digitalWrite(_pin_UD, LOW);
  delayMicroseconds(_tLUC_us);
  digitalWrite(_pin_CS, LOW);
  delayMicroseconds(_tLCUR_us);
  while(n_decrements > 0){   
    digitalWrite(_pin_UD, HIGH);
    delayMicroseconds(_tHI_us);
    digitalWrite(_pin_UD, LOW);
    delayMicroseconds(_tLO_us);
    n_decrements--;
  }  
  digitalWrite(_pin_CS, HIGH);  
}

void PotentiometerArduino::decrementSequenceAndSave(){
  int count = 0;
  digitalWrite(_pin_UD, LOW);
  delayMicroseconds(_tLUC_us);
  digitalWrite(_pin_CS, LOW);
  delayMicroseconds(_tLCUR_us);
  while(digitalRead(_pin_FC_ANALOG) && count < 100){   
    digitalWrite(_pin_UD, HIGH);
    delayMicroseconds(_tHI_us);
    digitalWrite(_pin_UD, LOW);
    delayMicroseconds(_tLO_us);
    count++;
  }
  digitalWrite(_pin_UD, HIGH);
  delayMicroseconds(_tHI_us);
  digitalWrite(_pin_CS, HIGH);

  delayMicroseconds(10000);
}

bool PotentiometerArduino::checkCalibration(){
  if(digitalRead(_pin_FC_ANALOG) == HIGH)
  {
    return false;
  }
  return true;
}

void continuarSerial(){
  byte c;

  Serial.print("Press \'y\' to continue:");
  while(c != 'y' && c != 'Y'){
    while(Serial.available() == 0){}
    
    Serial.print("\nPress \'y\' to continue:");
    c = Serial.read();
    serialFlush();    
  }
}

void serialFlush(){
  while(Serial.available() > 0) {
    char t = Serial.read();
  }
}   
