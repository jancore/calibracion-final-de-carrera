#include "Arduino.h"

#ifndef POTENTIOMETER_h
#define POTENTIOMETER_h
#define DEBUG 0

class IPotentiometer
{
  public:  
    virtual void incrementSequenceAndSave() = 0;
    virtual void decrementSequence(unsigned int n_decrements) = 0;
    virtual void incrementSequence(unsigned int n_increments) = 0;
    virtual void decrementSequenceAndSave() = 0;
    virtual bool checkCalibration() = 0;
};

class PotentiometerArduino : IPotentiometer
{
  public:
    PotentiometerArduino(unsigned int pin_CS, unsigned int pin_UD, unsigned int pin_FC_ANALOG, unsigned int maxIncrement) ;
    void setDelays(
      unsigned int tCSLO_us, unsigned int tCSHI_ns, unsigned int tLUC_ns, unsigned int tLCUF_ns, unsigned int tLCUR_us, unsigned int tHI_us, unsigned int tLO_us, unsigned int tS_us);
    void incrementSequenceAndSave() override ;
    void decrementSequence(unsigned int n_decrements) override;
    void incrementSequence(unsigned int n_increments) override;
    void decrementSequenceAndSave() override;
    bool checkCalibration() override;
  private:
    unsigned int _pin_CS, _pin_UD, _pin_FC_ANALOG;
    unsigned int _maxIncrement;
    unsigned int _tCSLO_us = 500;
    unsigned int _tCSHI_us = 100;
    unsigned int _tLUC_us = 100;
    unsigned int _tLCUF_us = 100;
    unsigned int _tLCUR_us = 300;
    unsigned int _tHI_us = 100;
    unsigned int _tLO_us = 100;
    unsigned int _tS_us = 100; 
};

void continuarSerial();
void serialFlush();
#endif
